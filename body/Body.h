#ifndef BALI_BODY_H
#define BALI_BODY_H
#include "GeometricObj.h"
#include "PhysicsObj.h"

namespace Bali {
    class Body : public GeometricObj, public PhysicsObj {
    public:
        Body();
        virtual ~Body();
    protected:
        
    };
}
#endif
