#ifndef BALI_PROPERTY_H
#define BALI_PROPERTY_H
//#include "LnLinkedList.h"

namespace Bali {
    class PropertyInstance;
    class PropertySet;
    
    class Property {
    friend class PropertySet;
    public:
        typedef Uint16 id_t;
        Property();
        virtual ~Property();
        
        _getters(id, name, abbrev);
        
        virtual PropertyInstance* alloc() = 0;
        virtual void dealloc(PropertyInstance* ins) = 0;
        
        static bool Register(Property *prop);
        inline static Property* Resolve(const std::string& str) {
            auto f = Index.find(str);
            if(f == Index.end()) return nullptr;
            else return f->second;
        }
    protected:
        Uint16 id_;
        
        std::string name_;
        std::string abbrev_;
        std::vector<id_t> requisites_, requiredBy_;
        _setters(name, abbrev, requisites, requiredBy);
        
        static std::unordered_map<std::string, Property*> Index;
    };
    /*
    class PropertyInstance;
    class PropertyInstance : public SortedLinkedListNode<PropertyInstance*> {
    friend class PropertySet;
    public:
        Property *property;
        
        inline PropertyInstance(Property *prop = nullptr) : property(nullptr) {
            if(prop != nullptr) this->setProperty(prop);
        }
        inline virtual ~PropertyInstance() {
            this->setProperty(nullptr);
        }
        
    protected:
        inline void setProperty(Property *prop) {
            //if(this->property != nullptr and this->property != prop) this->property->dealloc(this);
            else 
            if(this->property == prop) return;
            else this->property = prop;
            //if(prop == nullptr) return;
            //this->property->alloc(this);
        }
    };*/
}

#endif
