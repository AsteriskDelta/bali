#ifndef BALI_PROPERTYSET_H
#define BALI_PROPERTYSET_H
#include "Property.h"

namespace Bali {
    class PropertyApplicator;
    struct PropertyRecordHeader {
        PropertyApplicator *owner;
        ptr_t data;
    };
    
    class PropertySet {
    public:
        PropertySet();
        virtual ~PropertySet();
    protected:
        
    };
}

#endif
