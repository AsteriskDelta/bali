class TestOrder;
#include "Bali.h"
using namespace arke;
#include <iostream>
#include "LnLinkedList.h"

class TestOrder;
class TestOrder : public relmem_t<TestOrder*>, public LnLinkedListNode<TestOrder*> {
public:
    using Self=TestOrder;
    int value;
    
    TestOrder(int v = -1) : relmem_t<TestOrder*>(), LnLinkedListNode<TestOrder*>(reinterpret_cast<ptr_t>(this)), value(v) {
        std::cout << "allocated TestOrder to " << std::hex << this << std::dec << "\n";
        
    };
    
    RELMEM_CLASS;
    
    operator<(const TestOrder& o) const {
        return this->value < o.value;
    }
    operator>(const TestOrder& o) const {
        return this->value > o.value;
    }
    operator==(const TestOrder& o) const {
        return this->value == o.value;
    }
    
    inline auto body() {
        return this;
    }
    inline const auto body() const {
        return this;
    }
};


int main(int argc, char** argv) {
    _unused(argc, argv);
    LnLinkedList<TestOrder> list;
    for(int i = 0; i < 10; i++) {
        int val = rand()%100;
        TestOrder *newItem = new TestOrder(val);
        std::cout << "new testorder="<<val<<" alllocated in main()  at " << std::hex << newItem << std::dec << "\n";
        list.insert(newItem);
    }
    return 0;
}
