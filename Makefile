NAME = Bali

#Directory that contains .cpp files with main()
EXEC_DIRS = ./specs/

#directories with classes/files to be compiled into objects
SRC_DIRS = 

#directories containing headers that need to be installed
HEADER_DIRS = body physics geom properties lib
HEADER_CP_DIRS = 
HEADER_FILES += Bali.h BaliInclude.h
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = Bali

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
#PCH = CAIH.h

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS += `sdl2-config --cflags` `freetype-config --cflags` -fopenmp -fno-stack-protector -I/usr/include/freetype2/ --include BaliInclude.h
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?= -Wmaybe-uninitialized -Wuninitialized
#Optimization flags, supporting PGO if needed
OPTI ?= -march=native 
#OPTI += -Ofast
OPTI += -ffast-math -fno-strict-aliasing
#Include paths, ie -I/path/to/headers/
INCPATH += 
#Libraries, ie -lopenmp
#LIB_PATHS += ~/Projects/Dimensionals/build/Desktop/ ~/Projects/GNFCtrl/build/Desktop/
LIBS += -lGL -lGLEW -lGLU `sdl2-config --libs` -L./dyn/ -lassimp -lfreetype -lnoise -ldl
EXEC_LIBS += 

include /usr/include/LPBT.mk

debug::
#@mv `ls -1 | grep .so | grep -v libC3AI.so | tr "\n" " "` extensions/ 2>/dev/null || true

install::

uninstall::

autorun::

disable::


