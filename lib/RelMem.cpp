#include "RelMem.h"
#include <sys/mman.h>
#include <linux/mman.h>
#include <set>
#include "RelShared.h"

namespace arke {
    relmem_base_t::relmem_base_t() : objectSize(0) {
        
    }
    relmem_base_t::relmem_base_t(ptr_t objSize, ptr_t relmemOffset_, bool destructors) : objectSize(objSize), relmemOffset(relmemOffset_), holes(), callDestructors(destructors) {
        this->mapMemory();
        this->maxObjectCount = this->memorySize/this->objectSize;
    }
    relmem_base_t::~relmem_base_t() {
        if(!(*this)) return;
        if(this->callDestructors) this->destroyAll();
        this->unmapMemory();
    }
    
    ptr_t relmem_base_t::allocate() {
        if(this->holes.size() > 0) {
            ptr_t ret = holes.back();
            holes.pop_back();
            return ret;
        } else {
            if(_unlikely(this->edge >= this->memorySize)) {
                return OutOfMemory;
            }/* else if (_unlikely(this->objectCount == 0)) {
                this->objectCount++;
                this->edge.self += objectSize;
                return this->base;
            } */else {
                this->objectCount++;
                ptr_t ret = this->edge + this->base;
                this->edge.self += objectSize;
                return ret;
            }
        }
    }
    void overwrite(ptr_t at) {
        _unused(at);
    }
    void relmem_base_t::deallocate(ptr_t addr) {
        this->holes.push_back(addr);
    }

    ptr_t relmem_base_t::mapMemory() {
        int allow = PROT_READ |PROT_WRITE;
        int flags = MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE | MAP_UNINITIALIZED;
        this->memorySize = (static_cast<ptr_t>(0x1)<< (32)) - 1;//For memory-backed
        auto res = ::mmap(NULL, static_cast<size_t>(this->memorySize * 2), allow, flags, -1, 0);
        ptr_t memBase = (reinterpret_cast<ptr_t>(res) + this->memorySize) & ~this->memorySize;
        std::cout << "mmap returned "<< std::hex << res << ", trunc to "<<memBase<<"\n"<<std::dec;
        
        const ptr_t pageSize = ::getpagesize();
        ptr_t seg2Start = (memBase + this->memorySize);
        if(seg2Start % pageSize != 0) seg2Start = (seg2Start / pageSize + 1) * pageSize;
        
        ptr_t seg1Size = memBase - reinterpret_cast<ptr_t>(res),
              seg2Size = (reinterpret_cast<ptr_t>(res) + this->memorySize * 2) - seg2Start;
        
        std::cout << "unmapping regions " << std::hex << reinterpret_cast<ptr_t>(res) << " -> " << (reinterpret_cast<ptr_t>(res) + seg1Size) << " ("<<seg1Size<<"), "<<seg2Start << " -> " << (seg2Start + seg2Size) << " ("<<seg2Size<<")\n" << std::dec;
        
        bool unmappedOk = true;
        if(seg1Size > 0) {
            int uStatus = ::munmap(reinterpret_cast<void*>(res), reinterpret_cast<size_t>(seg1Size));
            //std::cout<<"uStatus1 = " << uStatus << "\n";
            unmappedOk &= uStatus == 0;
        }
        if(seg2Size > 0) {
            int uStatus = ::munmap(reinterpret_cast<void*>(seg2Start), reinterpret_cast<size_t>(seg2Size));
            //std::cout<<"uStatus2 = " << uStatus << "\n";
            unmappedOk &= uStatus == 0;
        }
        if(!unmappedOk) {
            std::cout << "Unable to fully unmap non-needed space\n";
        }
        
        this->base = memBase;//reinterpret_cast<ptr_t>(res);
        this->offBase = this->base + this->relmemOffset;
        this->edge = 0;
        this->objectCount = 0;
        return this->base;
    }
    void relmem_base_t::destroyAll() {
        std::unordered_set holeSet(this->holes.begin(), this->holes.end());
        uint32_t count = 0;
        for(ptr_t addr = 0; addr < this->edge; addr += this->objectSize) {
            bool empty = holeSet.find(addr) == holeSet.end();
            if(empty) continue;
            else {
                
                count++;
                if(count >= this->objectCount) break;
            }
        }
    }
    void relmem_base_t::unmapMemory() {
        void *ptr = reinterpret_cast<void*>(this->base);
        ::munmap(ptr, static_cast<size_t>(this->memorySize));
    }
    
};
