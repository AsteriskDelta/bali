#ifndef ARKE_RELPTR_H
#define ARKE_RELPTR_H
#ifndef _inl
#define _inl constexpr const
#endif

#define BASEPTR_SHIFT 32
//((ptr_t)sizeof(ptr_t)*8/2)

namespace arke {
    template<typename T> class RelPtrHolder;
    
    struct RelPtr {
        static _inl uint8_t MetaShift = 4;
        static _inl uint8_t PtrBits = 28;
        static _inl uint8_t PtrResolution = (0x1 << MetaShift) - 1;
        typedef RelPtr Self;
        union {
            struct {
                uint32_t self : 28;
                int8_t meta_ : 4;
            };
            uint32_t dat;
        };
        
        inline RelPtr() : self(0x0), meta_(0) {};
        inline RelPtr(ptr_t p, int8_t m = 0): self(static_cast<uint32_t>(p)), meta_(m) {};
        //inline RelPtr(T*const self_) : self(static_cast<Self>(self_)), meta(0){};
        
        inline operator ptr_t() {
            return static_cast<ptr_t>(self);
        }
        /*inline auto& operator=(const Self& o) {
            dat = o.dat;
            return *this;
        }
        inline auto& operator=(const ptr_t& o) {
            self = static_cast<decltype(self)>(o);
            return *this;
        }*/
        
        inline operator bool() const {
            return self != 0;
        }
        
        inline int8_t meta() const {
            return meta_;
        }
        inline int8_t meta(int8_t v) {
            return (meta_ = v);
        }
        
        template<typename K>
        inline typename std::remove_pointer<K>::type* get(const ptr_t& base) const {
            //std::cout << std::hex << "relptr_t.get off="<<self<<", base=" << base << ", bodyOffset="<< (RelPtrHolder<typename std::remove_pointer<K>::type*>::BodyOffset) << std::dec << "\n";
            return (
                 reinterpret_cast<typename std::remove_pointer<K>::type*>((ptr_t(base) << BASEPTR_SHIFT) + ptr_t(self) - RelPtrHolder<typename std::remove_pointer<K>::type*>::BodyOffset)
            );
        }
        template<typename K>
        inline ptr_t getRaw(const Self& base) const {
            return ((ptr_t(base) << BASEPTR_SHIFT) + ptr_t(self) - RelPtrHolder<K>::BodyOffset);
        }
        
        template<typename K>
        inline void set(K *const& base, K *const& ptr) {
            self = static_cast<Self>(self = 
                reinterpret_cast<ptr_t>(ptr) - (reinterpret_cast<ptr_t>(base) << BASEPTR_SHIFT)
            );
        }
    };
    using relptr_t = struct RelPtr;

    template<typename T>
    struct ResolvedRelPtr {
        using Self=ResolvedRelPtr<T>;
        using To = T;
        relptr_t base, off;
        
        inline ResolvedRelPtr() : base(), off() {
            
        }
        
        inline ResolvedRelPtr(const relptr_t& base_, const relptr_t& off_) : base(base_), off(off_) {
            //std::cout << std::hex << "ResolvedRelPtr() base=0x"<<base<<", off=0x"<<off<<"\n";
        };
        inline Self& operator=(To ptr) {
            _inl uint8_t shift = BASEPTR_SHIFT;
            _inl uint8_t mask_lsb_mask = (0x1 << 4) - 1;
            _inl ptr_t mask = ~((ptr_t(0xFFFFFFFF) << 32) | (mask_lsb_mask));
            const ptr_t v = reinterpret_cast<ptr_t>(ptr);
            base = relptr_t(v >> shift);
            off = relptr_t(v & mask);
            //std::cout << std::hex << "ResolvedRelPtr op= base=0x"<<base<<", off=0x"<<off<<"\n";
        }
        
        inline operator T() const {
            return off.get<To>(base);
        }
        
        inline To operator->() const {
            return off.get<typename std::add_const<To>::type>(base);
        }
        inline To operator->() {
            return  off.get<To>(base);
        }
        /*inline To operator*() const {
            return *off.get<decltype(*To())>(base);
        }*/
    };
    template<typename T>
    using rr_ptr_t = struct ResolvedRelPtr<T>;

    template<typename T>
    struct OffsetResolvedRelPtr {
        using Self=OffsetResolvedRelPtr<T>;
        using To = T;
        const relptr_t& base;
        relptr_t& off;
        
        inline OffsetResolvedRelPtr(const relptr_t& base_, relptr_t& off_) : base(base_), off(off_) {};
        inline Self& operator=(To *const& ptr) {
            _inl uint8_t shift = BASEPTR_SHIFT;
            _inl ptr_t mask = 0xFFFFFFFF & (~(0x1<<4)-1);
            const ptr_t v = reinterpret_cast<ptr_t>(ptr);
            off = static_cast<relptr_t>(v & mask);
        }
        
        inline To operator->() const {
            return off.get<To>(base)->body();
        }
        inline typename std::remove_pointer<To>::type & operator*() const {
            return *off.get<To>(base);
        }
    };
    template<typename T>
    using crr_ptr_t = struct OffsetResolvedRelPtr<T>;

    template<typename T> 
    struct BaseRelPtr : relptr_t {
        using To=T;
        inline BaseRelPtr() : relptr_t() {
        }
        inline BaseRelPtr(const relptr_t& ptr) : relptr_t(ptr) {
        }
        inline BaseRelPtr(const ptr_t& ptr) : relptr_t(ptr) {};
        
        //T bad = std::vector<int>();
        
        inline To operator+(const relptr_t& o) {
            return o.get<T>(this->self);
        }
        inline const rr_ptr_t<T> operator[](relptr_t& o) {
            return rr_ptr_t<T>(this->self, o.self);
        }
        inline const crr_ptr_t<T> operator[](relptr_t& o) const {
            return crr_ptr_t<T>(this->self, o.self);
        }
        inline relptr_t rel(T ptr) const {
            //std::cout << "BasePtr::rel(" << std::hex << ptr << ") => " << ptr << " - " << (ptr_t(this->self) << BASEPTR_SHIFT) << " = " << (reinterpret_cast<ptr_t>(ptr) - (ptr_t(this->self) << BASEPTR_SHIFT)) <<"\n";
            const relptr_t ret(reinterpret_cast<ptr_t>(ptr) - (ptr_t(this->self) << BASEPTR_SHIFT));
            
            //std::cout << "re-resolved = " << this->resolve(ret);
            return ret;
        }
        inline T resolve(const relptr_t& ptr) const {
            //std::cout << "RESOLVE base="<<std::hex << this->self << ", off="<<ptr.self<< "\n";
            //const rr_ptr_t<T> ret(this->self, ptr.self);
            auto ret = ptr.get<T>(this->self);
            //std::cout << "return "  << ret << "\n";
            return ret;//->body();
        }
        /*template<typename K>
        inline relptr_t makeRelative(K ptr) const {
            return relptr_t(reinterpret_cast<ptr_t>(ptr) - this->self);
        }*/
        /*template<typename K>
        inline relptr_t rel(K ptr) const {
            if constexpr(typeid(decltype(*ptr)) == typeid(T)) {
                return relptr_t(reinterpret_cast<ptr_t>(*ptr) - this->self);
            } else return relptr_t(reinterpret_cast<ptr_t>(ptr) - this->self);
        }
        template<typename K>
        inline relptr_t off(K ptr) const {
            if constexpr(typeid(decltype(*ptr)) == typeid(T)) {
                return relptr_t(reinterpret_cast<ptr_t>(*ptr) - this->self);
            } else return relptr_t(reinterpret_cast<ptr_t>(ptr) - this->self);
        }*/
        //inline const crr_ptr_t<To>(this->self, o.self);
    };
    template<typename T> 
    using base_relptr_t = struct BaseRelPtr<T>;
    
    template<typename T>
    class RelPtrHolder {
    public:
        typedef RelPtrHolder<T> Self;
        inline RelPtrHolder(const ptr_t &body) : base(reinterpret_cast<ptr_t>(body) >> BASEPTR_SHIFT) {
            std::cout << "Created RelPtrHolder with base=0x" << std::hex << this->base << " from body=0x"<<body<<std::dec<<"\n";
        }
        
        template<typename Z=void*>
        inline operator T() {
            return this->body();/*reinterpret_cast<T&>(*reinterpret_cast<T*>(
                reinterpret_cast<ptr_t>(this) - Self::BodyOffset)*/
            //);
        }
        template<typename Z=void*>
        inline operator const T() const {
            return this->body();
            //return static_cast<const T&>(*const_cast<const Self*>(this));
        }
        
        /*template<typename Z=void*>
        inline T body() {
            return (this->operator T());
        }
        template<typename Z=void*>
        inline const T body() const {
            return (this->operator const T());
        }*/
        base_relptr_t<T> base;
        
        static ptr_t BodyOffset;
    protected:
    };
};

#endif
