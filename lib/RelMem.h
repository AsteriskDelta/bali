#ifndef ARKE_RELMEM_H
#define ARKE_RELMEM_H
#include <vector>
namespace arke {
    class relmem_base_t;
    template<typename T>
    class relmem_t;
};

#define RELMEM_CLASS \
inline static void* operator new(std::size_t sz) {\
    _unused(sz);\
    void *const ret = reinterpret_cast<void*>(relmem_t<Self>::Allocate());\
    std::cout << "relmem new got " << std::hex << reinterpret_cast<void*>(relmem_t<Self>::Allocate()) << std::dec << "\n";\
    return ret;\
};\
inline static void* operator new(std::size_t sz, Self* at) {\
    _unused(sz);\
    at->selfDestruct();\
    return reinterpret_cast<void*>(at);\
};\
inline static void operator delete(void* ptr) {\
    relmem_t<Self>::Deallocate(reinterpret_cast<ptr_t>(ptr));\
};\
inline void selfDestruct() {\
    using X = Self;\
    this->~X();\
}\
inline Self* getSelf() {\
    return this;\
}\
inline const Self* getSelf() const {\
    return const_cast<Self*>(this)->getSelf();\
}
/*
#define RELMEM_CHILD_CLASS(BASE)\
inline _inl ROOT* getBase() const {\
    return 0;\
}
*/

#include "RelPtr.h"

namespace arke {
    class relmem_base_t {
    public:
        relmem_base_t();
        relmem_base_t(ptr_t objSize, ptr_t relmemOffset_ = 0, bool destructors = true);
        ~relmem_base_t();
        
        inline operator bool() const {
            return this->objectSize > 0;
        }
        
        inline ptr_t get(const ptr_t idx) const {
            return this->offBase + this->objectSize * idx;
        }
        
        ptr_t allocate();
        void overwrite(ptr_t at);
        void deallocate(ptr_t addr);
        void destroyAll();//Invokes destructors on all still-existing objects
        
        inline unsigned int size() {
            return this->objectCount;
        }
        
        inline bool full() const {
            return this->objectCount >= this->maxObjectCount;
        }
        
        static _inl ptr_t OutOfMemory = ((static_cast<uint64_t>(0x1) << 63) - 1) | (static_cast<uint64_t>(0xF) << 63);
        
        ptr_t base;
    protected:
        ptr_t mapMemory();
        void unmapMemory();
        
        ptr_t objectSize;
        ptr_t memorySize;
        ptr_t relmemOffset, offBase;
        relptr_t edge;
        unsigned int objectCount, maxObjectCount;
        std::vector<relptr_t> holes;
        bool callDestructors;
    };
    
    template<typename T>
    class relmem_t {
        friend class relmem_base_t;
    public:
        typedef relmem_base_t Base;
        typedef relmem_t<T> Self;
        typedef typename std::remove_pointer<T>::type Obj;
        //relmem_common_t();
        //~relmem_common_t();
        static bool CallDestructors;
        
        inline Self* relmem_tOffset() {
            return this;
        }
        
    protected:
        static _inl uint8_t MaxBaseCount = 16;
        static Base Bases[MaxBaseCount];
        static uint8_t BaseCount;
        static ptr_t BodyOffset;
        
        template<typename V=nullptr_t>
        inline void selfDestruct() {
            T* par = reinterpret_cast<T*>(
                reinterpret_cast<ptr_t>(this)
                - BodyOffset
            );
            par->~T();
        }
    public:
        inline static ptr_t Allocate() {
            for(uint8_t i = 0; i < BaseCount; i++) {
                if(!Bases[i].full()) return Bases[i].allocate();
            }
            
            if(BaseCount >= 1/*MaxBaseCount*/) throw std::bad_alloc();
            
            std::cout << "Creating new membase...\n";
            new (&Bases[BaseCount]) Base(sizeof(typename std::remove_pointer<T>::type), BodyOffset, CallDestructors);
            return Bases[BaseCount++].allocate();
        }
        inline static void Deallocate(ptr_t addr) {
            const ptr_t basePtr = addr & (ptr_t(0xFFFFFFFF) << 32);
            
            for(uint8_t i = 0; i < BaseCount; i++) {
                if(Bases[i].base == basePtr) Bases[i].deallocate(addr);
            }
        }
        inline static void Deallocate(T addr) {
            Deallocate(reinterpret_cast<ptr_t>(addr));
        }
        inline static void DeallocateAll() {
            for(uint8_t i = 0; i < BaseCount; i++) {
                Bases[i].destroyAll();
            }
        }
        /*
        inline Base* GetBase(uint8_t idx = 0xFF) {
            if(idx != 0xFF && idx < BaseCount) return &Bases[idx];
            else if(idx == 0xFF) {
                
            } else return nullptr;
        }*/
    };

    
    /*
    template<typename T>
    class relmem_t : protected relmem_common_t<T> {
    public:
        using SelfPtr = T;
        using RelMem = relmem_common_t<T>;
        
        inline relmem_t() {
            
        }
        inline ~relmem_t() {
            
        }
    protected:
        
    };*/
};

#include "RelShared.h"
#endif
