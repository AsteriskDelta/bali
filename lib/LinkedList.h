#ifndef ARKE_LINKEDLIST_H
#define ARKE_LINKEDLIST_H

namespace arke {
    template<typename T>
    class LinkedList {
    public:
        typedef LinkedList<T> Self;
        _getters(prev, next);
        
        template<void*>
        inline operator T&() {
            return reinterpret_cast<T&>(*reinterpret_cast<T*>(
                reinterpret_cast<ptr_t>(this) - Self::BodyOffset)
            );
        }
        template<void*>
        inline operator const T&() const {
            return static_cast<T&>(*const_cast<Self*>(this));
        }
        
        inline Self* start() const {
            if(this->prev() == nullptr) return this;
            else return this->prev()->start();
        }
        inline Self* end() const {
            if(this->next() == nullptr) return this;
            else return this->next()->end();
        }
        
        inline Self* prepend(Self *const o) {
            auto *const s = this->start();
            s->prev_ = o;
            o->next_ = s;
            return o;
        }
        inline Self* append(Self *const o) {
            auto *const e = this->start();
            o->prev_ = e;
            e->next_ = o;
            return o;
        }
        
        inline Self* insertBefore(Self *const toInsert, Self *const target, Int8 dir = 0) {
            if(this == target) {
                Self::Splice(this->prev(), toInsert, this);
                return toInsert;
            }
            
            if(dir == 0) {
                Self* a = this->insertBefore(toInsert, target, -1);
                if(a != nullptr) return a;
                else return this->insertBefore(toInsert, target, 1);
            }
            
            if(dir < 0) return this->prev() == nullptr?nullptr : this->prev()->insertBefore(toInsert, target, -1);
            else return this->next() == nullptr? nullptr : this->next()->insertBefore(toInsert, target,  1);
        }
        
        bool remove(Self *const target, Int8 dir = 0) {
            if(this == target) {
                Self::Splice(this->prev(), this->next);
                return true;
            }
            
            if(dir == 0) {
                auto a = this->remove(target, -1);
                if(bool(a)) return a;
                else return this->remove(target, 1);
            }
            
            if(dir < 0) return this->prev() == nullptr? false : this->prev()->remove(target, -1);
            else return this->next() == nullptr? false : this->next()->remove(target,  1);
        }
        
        inline Self* insert(Self *const toInsert, Int8 dir = 1) {
            if(dir < 0) return this->insertBefore(toInsert, this, -1);
            else return this->insertAfter(toInsert, this, 1);
        }
        
        inline Self* insertAfter(Self *const toInsert, Self *const target, Int8 dir = 0) {
            if(this == target) {
                Self::Splice(this, toInsert, this->next);
                return toInsert;
            }
            
            if(dir == 0) {
                Self* a = this->insertAfter(toInsert, target, -1);
                if(a != nullptr) return a;
                else return this->insertAfter(toInsert, target, 1);
            }
            
            if(dir < 0) return this->prev() == nullptr?nullptr : this->prev()->insertAfter(toInsert, target, -1);
            else return this->next() == nullptr? nullptr : this->next()->insertAfter(toInsert, target,  1);
        }
        
        Uint16 count(Int8 dir = 0) const {
            Uint16 cnt = 1;
            if(dir <= 0 && this->prev() != nullptr) cnt += this->prev()->count(-1);
            if(dir >= 0 && this->next() != nullptr) cnt += this->next()->count( 1);
            return cnt;
        }
        
        static Self* Splice(Self *a, Self *b, Self *c) {
            if(a != nullptr) a->next_ = b;
            b->prev_ = a;
            b->next_ = c;
            if(c != nullptr) c->prev_ = b;
            return b;
        }
        static bool Splice(Self *a, Self *c) {
            if(a != nullptr) a->next_ = c;
            if(c != nullptr) c->prev_ = a;
            return true;
        }
        static ptr_t BodyOffset;
    protected:
        Self* prev_, next_;
        
    };
    
    template<typename T>
    ptr_t LinkedList<T>::BodyOffset = []() {
        T* t = reinterpret_cast<T*>(0xFFFFFFFF);
        LinkedList<T>* l = (LinkedList<T>*)t;
        return (reinterpret_cast<ptr_t>(t) - reinterpret_cast<ptr_t>(l));
    };
}



#endif
