#ifndef ARKE_LNLINKEDLIST_H
#define ARKE_LNLINKEDLIST_H
#include "RelMem.h"
#include "RelPtr.h"
#include <type_traits>
#include <iostream>
#include <sstream>
//For ease of experimenting with optimizations
#define ACCESS_OFFSET_RRPTR_SEP(NM, VN) \
    inline Obj* NM() const {\
        std::cout << "\nrecon "<<#NM<<" from base=" <<std::hex<< this->base.self << " ptr=" << ptrs.VN.self <<std::dec<< "\n";\
        if(!ptrs.VN) return nullptr;\
        auto ret = ptrs.VN.template get<Obj*>(this->base.self);\
        std::cout << "\tret="<<std::hex<<ret<<std::dec<<"\n";\
        return ret;\
    };\
    inline relptr_t NM(Obj *const n) {\
        ptrs.VN = this->base.rel(n);\
        std::cout << "\ndecompose "<<#NM<<" from base=" << this->base << " off="<<n<<" resulting in ptr=" << ptrs.VN << "\n";\
        return ptrs.VN;\
    };

//Helper Macros
#define ACCESS_OFFSET_RRPTR(NM) ACCESS_OFFSET_RRPTR_SEP(NM, NM)
#define ACCESS_OFFSET_RRPTRS(...) APPLYXn(ACCESS_OFFSET_RRPTR, __VA_ARGS__)

/*
 *                 R       
 * |------Rl------|^|-------Rr------|
 *               Rp Rn
 *   Pl Pp^^Pn Pr
 */
namespace arke {
    template<typename T>
    class SortedLinkedListNode : public RelPtrHolder<T> {
    public:
        typedef typename std::remove_pointer<T>::type Obj;
        typedef SortedLinkedListNode<T> Self;
        typedef rr_ptr_t<T> rptr;
        typedef relmem_t<T> rmem;
        
        
        SortedLinkedListNode() = delete;
        
        SortedLinkedListNode(const ptr_t& pPtr) : RelPtrHolder<T>(pPtr) , ptrs() {
            std::cout << "created SortedLinkedListNode with ptr="<<std::hex<<pPtr<<std::dec<<"\n";
        }
        
        bool operator<(const T& o) = delete;
        bool operator>(const T& o) = delete;
        bool operator==(const T& o) = delete;
        
        static inline _inl int8_t UpperMedianThreshold = 2;
        static inline _inl int8_t LowerMedianThreshold = 1;
        /*inline operator T&() {
            return *static_cast<T>(this);
        }
        inline operator const T& () const {
            return *static_cast<const T>(this);
        }*/
        static ptr_t BodyOffset;
        inline operator T() {
            return reinterpret_cast<T>(
                reinterpret_cast<ptr_t>(this->body()) - BodyOffset
                               );
        }
        inline operator const T () const {
            return const_cast<Self*>(*this)->operator T();
        }
        inline T operator->() {
            return static_cast<T>(*this);
        }
        inline const T operator-> () const {
            return static_cast<const T>(*this);
        }
        /*
        template <class K>
        operator K*() {
            return static_cast<K*>(this);
        }
        template <class K>
        operator const K*() const {
            return static_cast<const K*>(this);
        }*/
        
        ACCESS_OFFSET_RRPTRS(
            leftMedian, 
            prev, 
            parent, 
            next, 
            rightMedian
        );
        //ACCESS_OFFSET_RRPTR_SEP(self, base.rel(this));
        inline relptr_t self() const {
            return this->base.rel(this->body());
        }
        
        inline ptr_t rel(Obj* obj) const {
            return this->base.rel(obj);
        }
        inline ptr_t rel(Self* obj) const {
            return this->base.rel(obj->body());
        }
        
        inline rptr start() const {
            if(this->parent() != nullptr && this->parent()->leftMedian()) {
                return this->parent()->leftMedian()->start();
            } else if(this->leftMedian() != nullptr) {
                return this->leftMedian()->start();
            } else if(this->prev() != nullptr) return this->prev()->start();
            else return this;
        }
        inline rptr end() const {
             if(this->parent() != nullptr && this->parent()->rightMedian()) {
                return this->parent()->rightMedian()->end();
            } else if(this->rightMedian() != nullptr) {
                return this->rightMedian()->end();
            } else if(this->next() != nullptr) return this->next()->end();
            else return this;
        }
        
        void shift(int8_t dir) {
            int8_t m = this->base.meta();
            m += dir;
            if(abs(m) < UpperMedianThreshold) return;
            
            while(abs(m) > LowerMedianThreshold) {
                Self* to = (m > 0)? this->prev() : this->next();
                Self* newParent = to->parent();
                if(newParent == nullptr) newParent = to;//to is root
                this->parent()->replaceMedian(this, to);
                relptr_t &targetMedian = *((m < 0)? &newParent->ptrs.leftMedian : &newParent->ptrs.rightMedian);
                targetMedian = newParent->base.rel(this->body());
                this->ptrs.parent = this->rel(newParent->body());
                m -= dir;
            }
            this->base.meta(m);
        }
        
        Obj* seek(int16_t dir) {
            Obj* current = this->body();//self();
            for(int16_t i = 1; i <= dir; i++) {
                current = current->next();
            }
            for(int16_t i = -1; i >= dir; i--) {
                current = current->prev();
            }
            return current;//->body();
        }
        
        bool remove(Self *const target) {
            _unused(target);
            return false;
        }
        
        T_BODY_FN();
        
        inline Self* root() {
            Self* ret = this->parent();
            if(ret == nullptr) return this;
            else return ret;
        }
        
        template<typename N=nullptr_t>
        inline Obj* insert(Obj *const toInsert, int8_t dir = 0) {
            int8_t relDir = 0;
            Obj* ret;
            std::cout <<std::hex<< "insert " << toInsert << ", lMed=" << this->ptrs.leftMedian.self << ", rMed = " << this->ptrs.rightMedian.self << std::dec<<"\n";
            if(toInsert->body() < this->body()) {
                std::cout << "\t"<<toInsert->body()->value << " < " << this->body()->value << "\n";
                relDir = -1;
                usleep(1000*250);
                if(!this->ptrs.leftMedian) goto siblingInsert;
                else ret = this->leftMedian()->insert(toInsert, dir);
            } else if(toInsert->body() > this->body()) {
                std::cout << "\t"<<toInsert->body()->value << " > " << this->body()->value << "\n";
                relDir = 1;
                usleep(1000*250);
                if(!this->ptrs.rightMedian) goto siblingInsert;
                else ret = this->rightMedian()->insert(toInsert, dir);
            } else {
                std::cout << "\t"<<toInsert->body()->value << " == " << this->body()->value << "\n";
                goto fRet;
            }
            
            siblingInsert: {
                relptr_t &sibling = *(&ptrs.parent + relDir), &median = *(&ptrs.parent + relDir*2);
                std::cout << "raw sibling: " << sibling.self << ", raw median: " << median.self << "\n";
                Obj* rSibling = this->base.resolve(sibling);
                std::cout << "For lNode base=" << std::hex<< this->base.self << " toInsert = " << toInsert << "resolvedSibling = " << std::hex << rSibling << std::dec << "\n";
                std::cout << "resolves to " << std::hex << this->base.rel(rSibling) << std::dec << "\n";
                //If no sibling, simply assign it
                if(!sibling) {
                    rSibling = toInsert;
                    sibling =  this->base.rel(toInsert);
                    relptr_t *sibPtr = reinterpret_cast<relptr_t*>(&rSibling->ptrs.parent - relDir* sizeof(relptr_t));
                    std::cout << "this->base = " << std::hex << this->base.self << ", sibPtr = " << sibPtr << " ";
                    std::cout << ", " << "body = " << this->body() << std::dec<< "\n";
                    *sibPtr = this->base.rel(this->body());
                    ret = toInsert;
                } else {
                    Obj* rSiblingObj = rSibling->body();
                    int8_t repDir = (int8_t(*rSiblingObj > *toInsert) * 2 - 1) * relDir;
                    Splice(rSibling, toInsert, repDir);
                    median = this->base.rel(this->seek(relDir*2));//Assign new "median"
                    (this->base+median)->parent(this->body());
                    ret = toInsert;
                }
            };
            fRet:
            usleep(1000*250);
            this->shift(-relDir);
            return ret->body();
        }
        
        /*Uint16 size() const {
            
        }*/
        
        struct {
            relptr_t leftMedian = 0,
            prev = 0,
            parent = 0,
            next = 0,
            rightMedian = 0;
        } ptrs;
        
        /*
        inline relptr_t* getLinks() {
            return reinterpret_cast<relptr_t*>(&this->ptrs);
        }
        inline _inl uint8_t getLinkCount() const {
            return 5;
        }*/
    protected:
    
    private:
        inline bool replaceMedian(Self *const old_, Self *const new_) {
            relptr_t o(this->base.rel(old_->body())), n(this->base.rel(new_->body()));
            if(o == ptrs.leftMedian) {
                ptrs.leftMedian = n;
                return true;
            } else if(o == ptrs.rightMedian) {
                ptrs.rightMedian = n;
                return true;
            } else return false;
        }
        
        //WARNING: All splice functions assume shift-balancing has already taken place
        static void Splice(Self *const base, Self *const new_, int8_t dir) {
            auto basePtr = base->body();
            auto newPtr = new_->body();
            
            if(dir > 0) {
                Obj *const next = base->next()->body();
                base->ptrs.next = basePtr->rel(newPtr);
                new_->ptrs.prev = newPtr->rel(basePtr);
                if(next != nullptr) {
                    new_->ptrs.next = newPtr->rel(next);
                    next->ptrs.prev = next->rel(newPtr);
                }
            } else {
                Obj *const prev = base->prev()->body();
                base->ptrs.prev = base->rel(newPtr);
                new_->ptrs.next = new_->rel(basePtr);
                if(prev != nullptr) {
                    new_->ptrs.prev = new_->rel(prev);
                    prev->ptrs.next = prev->rel(newPtr);
                }
            }
        }
        
        static void SpliceOut(Self *const toRemove) {
            Self *const before = toRemove->prev(), *const after = toRemove->next();
            if(before != nullptr) before->ptrs.next = after;
            if(after != nullptr) after->ptrs.prev = before;
            
        }
    };
    template<typename T>
    using lnlist_node_t = SortedLinkedListNode<T>;
    
    DECLARE_BODY_OFFSET(SortedLinkedListNode);
    
    template<typename T>
    class LnLinkedList {
    public:
        typedef typename std::remove_pointer<T>::type Obj;
        class Iterator {
        public:
            
        protected:
            
        };
        
        inline LnLinkedList() : root(nullptr), size_(0) {
            
        }
        inline ~LnLinkedList() {
            
        }
        
        inline T* first() const {
            return this->root->start();
        }
        inline T* last() const {
            return this->root->end();
        }
        
        inline Obj* insert(Obj *const val) {
            Obj* ret _safety( = nullptr);
            if(root == nullptr) {
                ret = static_cast<Obj*>(this->root = val);
            } else {
                ret = static_cast<Obj*>(root->insert(val));
                this->checkRoot();
            }
            
            _safety(if(ret != nullptr)) this->size_ += 1;
            return ret;
        }
        
        inline bool remove(T *const ptr) {
            _safety( if(this->root == nullptr) return false;)
            bool ret = this->root->remove(ptr);
            if(ret) this->size_ -= 1;
            return ret;
        }
        
        inline const uint64_t& size() const {
            return this->size_;
        }
    protected:
        SortedLinkedListNode<typename std::add_pointer< T>::type> *root;
        uint64_t size_;
        //decltype(root) bad = 453;//Correct
        inline void checkRoot() {
            this->root = this->root->root();
        }
        
    };
    template<typename T>
    using LnLinkedListNode=SortedLinkedListNode<T>;
}



#endif
