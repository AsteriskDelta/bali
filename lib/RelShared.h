#ifdef ARKE_RELMEM_H
#ifdef ARKE_RELPTR_H
#ifndef ARKE_RELSHARED_H
#define ARKE_RELSHARED_H

#define DECLARE_BODY_OFFSET(TT) \
template<typename T>\
    ptr_t TT<T>::BodyOffset = []() -> ptr_t {\
        typedef T Target;\
        typedef TT<T> Superclass;\
        Target* t = reinterpret_cast<Target*>(\
            ptr_t(0xFFFFFFFF)\
        );\
        Superclass* l = ((Superclass*)t);\
        return (reinterpret_cast<ptr_t>(t) - reinterpret_cast<ptr_t>(l));\
    }();
    
#define T_BODY_FN() \
    template<typename V=nullptr_t>\
        inline typename std::remove_pointer<T>::type* body(const Self& base) const {\
            return reinterpret_cast<typename std::remove_pointer<T>::type*>(\
                reinterpret_cast<ptr_t>(this)\
                - BodyOffset\
             + base.base);\
        };\
    template<typename V=nullptr_t>\
        inline typename std::remove_pointer<T>::type* body() {\
            return reinterpret_cast<typename std::remove_pointer<T>::type*>(reinterpret_cast<ptr_t>(this) \
                - BodyOffset);\
        };

namespace std {
    template <>
    struct hash<arke::relptr_t> {
    std::size_t operator()(const arke::relptr_t& ptr) const {
            return static_cast<std::size_t>(ptr.dat);
        }
    };
};

namespace arke {
    template<typename T>
    relmem_base_t relmem_t<T>::Bases[16];
    template<typename T>
    uint8_t relmem_t<T>::BaseCount = 0;
    template<typename T>
    bool relmem_t<T>::CallDestructors = true;
    /*
    template<typename T>
    ptr_t relmem_t<T>::BodyOffset = []() -> ptr_t {
        typedef typename T::Obj Target;
        typedef relmem_t<T> Superclass;
        Target* t = reinterpret_cast<Target*>(
            ptr_t(0xFFFFFFFF)
        );
        Target& tRef = *t;
        Superclass* l = &((Superclass&)tRef);
        //relmem_t<typename T::Obj*>* l = &static_cast<relmem_t<T>&>(*t);
        return (reinterpret_cast<ptr_t>(t) - reinterpret_cast<ptr_t>(l));
    }();*/
    DECLARE_BODY_OFFSET(relmem_t);
    DECLARE_BODY_OFFSET(RelPtrHolder);
    /*
    template<typename T>
    ptr_t RelPtrHolder<T>::BodyOffset = []() -> ptr_t {
        T* t = reinterpret_cast<T>(
            ptr_t(0xFFFFFFFF)
        );
        auto l = *static_cast<RelPtrHolder<typename T::Obj>&>(
            static_cast<typename T::Obj &>(*t)
        );
        return (reinterpret_cast<ptr_t>(t) - reinterpret_cast<ptr_t>(l));
    }();*/
};


#endif
#endif
#endif
